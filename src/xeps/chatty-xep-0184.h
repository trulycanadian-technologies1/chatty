/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef __XEPS_H_INCLUDE__
#define __XEPS_H_INCLUDE__

#include "purple.h"


void chatty_xeps_init (void);
void chatty_xeps_close (void);

#endif
