/*
 * Copyright (C) 2018 Purism SPC
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


#ifndef __POPOVER_H_INCLUDE__
#define __POPOVER_H_INCLUDE__

#include <gtk/gtk.h>

void chatty_popover_actions_init (GtkWindow *window);

#endif
